# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/oleg/project/client/src/adb_generateTopicEventRequest.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_generateTopicEventRequest.c.o"
  "/home/oleg/project/client/src/adb_generateUnsubscribeEventRequest.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_generateUnsubscribeEventRequest.c.o"
  "/home/oleg/project/client/src/adb_generateUnsubscribeEventResponse.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_generateUnsubscribeEventResponse.c.o"
  "/home/oleg/project/client/src/adb_message.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_message.c.o"
  "/home/oleg/project/client/src/adb_subscribeResponse.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_subscribeResponse.c.o"
  "/home/oleg/project/client/src/adb_subscriber.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_subscriber.c.o"
  "/home/oleg/project/client/src/adb_subscriberRequest.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_subscriberRequest.c.o"
  "/home/oleg/project/client/src/adb_topic.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_topic.c.o"
  "/home/oleg/project/client/src/adb_unsubscribeRequest.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_unsubscribeRequest.c.o"
  "/home/oleg/project/client/src/adb_unsubscribeResponse.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/adb_unsubscribeResponse.c.o"
  "/home/oleg/project/client/src/axis2_extension_mapper.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/axis2_extension_mapper.c.o"
  "/home/oleg/project/client/src/axis2_stub_Subscribers.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/axis2_stub_Subscribers.c.o"
  "/home/oleg/project/client/src/subscribers_client.c" "/home/oleg/project/client/CMakeFiles/main.dir/src/subscribers_client.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "client/src"
  "/home/oleg/axis2c/include/axis2-1.6.0"
  "/home/oleg/axis2c/include/axis2-1.6.0/platforms"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
